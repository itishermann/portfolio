---
title: How to use SwiftUI Gauge and create custom gauge style in iOS 16
subtitle: In this tutorial, we'll check out SwiftUI Gauge views, which is a new feature introduced in iOS 16. You'll learn how to use Gauge to create a temperature meter.
layout: src/layouts/BlogPostLayout.astro
date: 2022-10-01T14:17:19.610Z
tags: ["SwiftUI", "iOS 16"]
cover: /static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/cover.png
---

SwiftUI includes a new view named `Gauge` for displaying progress in iOS 16. It is used to display values inside a range. Let's look at how to use the Gauge view and deal with different gauge styles in this tutorial.  
But first let's take a look at Apple's official definition for the Gauge view.

> A gauge is a view that shows a current level of a value in relation to a specified finite capacity, very much like a fuel gauge in an automobile. Gauge displays are configurable; they can show any combination of the gauge’s current value, the range the gauge can display, and a label describing the purpose of the gauge itself.
> 
>[Apple Developper documentation](https://developer.apple.com/documentation/swiftui/gauge)

## Simplest Gauge
The simplest way to use the Gauge view is to pass a value to the `init` method. In it's most basic form, the value of the gauge is in `0.0` to `1.0` range.

```swift
// Most basic usage of the Gauge view
struct ContentView: View {
    var body: some View {
        Gauge(value: 0.5) {
            Text("Gauge")
        }
    }
}
``` 
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/basic-gauge-view-light-mode.png" alt="GaugeView in Light Mode"/>
    <figcaption>GaugeView in Light Mode</figcaption>
</figure>
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/basic-gauge-view-dark-mode.png" alt="GaugeView in Dark Mode"/>
    <figcaption>GaugeView in Dark Mode</figcaption>
</figure>

## Gauge with a range
You can also make the Gauge view display a value as a percentage of a range. The value must be between the range's minimum and maximum values. The Gauge view will display the value as a percentage of the range.
```swift
// Gauge view displaying a value as a percentage of a range
struct ContentView: View {
    var body: some View {
        Gauge(value: 25, in: 0...100) {
            Text("25% Gauge")
        }
    }
}
```

<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/25-percent-gauge-light-mode.png" alt="GaugeView in Light Mode">
    <figcaption>25% GaugeView in Light Mode</figcaption>
</figure>
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/25-percent-gauge-dark-mode.png">
    <figcaption>25% GaugeView in Dark Mode</figcaption>
</figure>

## Gauge styles
You can customize the Gauge view by passing a `GaugeStyle` to the `gaugeStyle` modifier. The `GaugeStyle` protocol defines the appearance of the Gauge view. There are two default styles provided by SwiftUI: `LinearGaugeStyle` and `RadialGaugeStyle`.
The available gaugeStyles are:
- `.accessoryCircularCapacity`
- `.accessoryCircular`
- `.accessoryLinearCapacity`
- `.accessoryLinear`
- `.linearCapacity`
- `.automatic`
```swift
// Gauge view displaying a value as a percentage of a range
struct ContentView: View {
    var body: some View {
        Gauge(value: 25, in: 0...100) {
            Text("Label")
        }
        .gaugeStyle(.accessoryLinearCapacity)
    }
}
```
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/different-gauge-view-in-light-mode.png" alt="GaugeView in Light Mode">
    <figcaption>Different GaugeView styles in Light Mode</figcaption>
</figure>
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/different-gauge-view-in-dark-mode.png">
    <figcaption>Different GaugeView styles in Dark Mode</figcaption>
</figure>

## Custom Gauge style
You can also create your own custom gauge style by conforming to the `GaugeStyle` protocol. The `GaugeStyle` protocol defines the appearance of the Gauge view.

```swift
// Custom GaugeStyle
struct CustomGaugeStyle: GaugeStyle {
    func makeBody(configuration: GaugeStyleConfiguration) -> some View {
        ZStack {
            Circle()
                .stroke(Color.gray, lineWidth: 10)
                .frame(width: 100, height: 100)
            Circle()
                .trim(from: 0, to: CGFloat(configuration.value))
                .stroke(Color.blue, lineWidth: 10)
                .frame(width: 100, height: 100)
                .rotationEffect(Angle(degrees: -90))
            Text("\(Int(configuration.value * 100))%")
                .font(.title)
        }
    }
}
```

```swift
// Custom GaugeStyle usage
struct ContentView: View {
    var body: some View {
        Gauge(value: 0.5) {
            Text("Gauge")
        }
        .gaugeStyle(CustomGaugeStyle())
    }
}
```

<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/custom-gauge-light-style.png" alt="GaugeView in Light Mode">
    <figcaption>Custom styles in Light Mode</figcaption>
</figure>
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/custom-gauge-dark-style.png">
    <figcaption>Custom styles in Dark Mode</figcaption>
</figure>

## Our Temperature Gauge
And finally let's make a nice looking temperature gauge. We will use the `accessoryLinear` gauge style and a nice gradient `tint` modifier.

```swift
struct TemperatureGaugeView: View {
    private var min: Double = 7.0
    private var max: Double = 22.6
    private var current: Double = 12.6
    var body: some View {
        Gauge(value: current, in: min...max) {
            Text("Tinted Gauge")
        }
        currentValueLabel: {
            Text("\(Int(current))°")
        }
        minimumValueLabel: {
            Text("\(Int(min))°")
        } maximumValueLabel: {
            Text("\(Int(max))°")
        }
            .tint(Gradient(colors: [.green, .yellow, .orange, .red]))
            .gaugeStyle(.accessoryLinear)
    }
}
```

<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/temperature-gauge-light.png" alt="GaugeView in Light Mode">
    <figcaption>Temperature Gauge Light Mode</figcaption>
</figure>
<figure>
    <img src="/static/how-to-use-swiftui-gauge-and-create-custom-gauge-style-in-iOS-16/temperature-gauge-dark.png">
    <figcaption>Temperature Gauge Dark Mode</figcaption>
</figure>

## Conclusion
I hope this tutorial helped you understand how to use the Gauge view in SwiftUI. If you have any questions, [please send me a little message](/contact), I will be happy to help you. If you liked this tutorial, please share it with your friends. Thank you for reading!