---
title: Correct file permission for ssh keys and folders
subtitle: Fixing permissions of files in ssh folder
layout: src/layouts/BlogPostLayout.astro
date: 2022-09-07T22:07:52.000Z
tags: ["Linux", "SSH"]
draft: false
---
I recently got an error on my work machine while restoring a backup of my ssh keys. The error stated that the permissions were too open for my private key. 
```bash
Permissions 0777 for '/home/<user>/.ssh/id_rsa' are too open.
```
Following an article from Stackoverflow i've set the access rights of all the files in the .ssh folder to 600. This solved the issue but is actually not better, because each file has it’s own permission for a reason. We set read only permission on these files for security reasons. Some are even changed by your ssh client and need a different permission due to that.

**Fixing these permissions**

These commands can help you get your permissions right
```bash
chown -R $USER:$USER ~/.ssh
chmod 700 ~/.ssh
chmod 644 ~/.ssh/authorized_keys
chmod 644 ~/.ssh/known_hosts
chmod 644 ~/.ssh/config
chmod 600 ~/.ssh/id*
chmod 644 ~/.ssh/id*.pub
```
You may need to use sudo to avoid errors.

I hope this helped you 😉. If you have any questions, [please send me a little message](/contact). I will be happy to help you. If you liked this tutorial, please share it with your friends. Thank you for reading!