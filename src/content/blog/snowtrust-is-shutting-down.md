---
title: Snowtrust is shutting down
subtitle: 3 years later, we are shutting down Snowtrust.
layout: src/layouts/BlogPostLayout.astro
date: 2022-10-05T17:37:19.610Z
tags: ["Announcement"]
cover: /static/snowtrust-is-shutting-down/cover.png
draft: false
---

Hello everyone,  
Long story short, i can no longer develop or maintain this service. It has become impossible due to my personal time constraints and studies.  
Thank you all who gave Snowtrust a shot and provided me with priceless feedback and education.
## What will happen? 
All servers, volumes, IPs and DNS records will be deleted on **2023 April 1st**. To prevent data loss I sent you an email with:
- All your invoices
- All your SEO reports
- All your case studies (if you have some)
- All your source code and backups of your website or web app
- All your database backups
## What's next? 
While Snowtrust will be soon gone, there are plenty of web agencies you can reach out to today. To name a few:
- [Studio Lumia](https://studio-lumia.fr) providing you unbelievable photo-shoots 
- Wordpress hosting provided by [Cloud Lumia](https://cloud.studio-lumia.fr)
- Marketing strategy provided by [Etre commerçant](https://etre-commercant.fr)

In case of any questions feel free to [reach out to me](/contact)  

Best,  
Hermann
