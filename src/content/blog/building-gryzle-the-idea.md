---
title: Building Gryzle - The idea
subtitle: What is Gryzle? Why did I build it? What does it do?
layout: src/layouts/BlogPostLayout.astro
date: 2022-10-08T10:30:12.899Z
tags: ["Gryzle", "Bootstrapping"]
cover: /static/building-gryzle-the-idea/cover.png
---

## It is 2014
I am 15 years old, I discover forex trading. I discovered a world where people spoke a different language with incomprehensible words. I often heard MetaTrader, support, resistance, etc... It was for me an easy way to get into investing with a small start. But it was necessary to get trained. So I read two books that are still my references in the field:
- __*Le chartisme*__ by François Baron
- __*Day Trading and Swing Trading the Currency Market: Technical and Fundamental Strategies to Profit from Market Moves*__ by Kathy Lien

Following this, I started with a demo account, an account where you can trade but with virtual money. I managed to reach a 70% profit in 2 months, and I could already see myself rich. So I decided that I was ready to start with a real account. I had a reality check when I was asked to verify that I was of age.
So I asked a friend to create an account for me, and fortunately, he accepted. I now had an account. I started with 25K francs (about 35€). In my first month, I made 1.3€ of profit. I started to lose hope, and I started to look for people who do online market analysis. I came across some signal providers. The principle was simple, you pay a monthly subscription and they send you the buy or sell orders and instruments on telegram. I thought I had found the holy grail. 
I took the first subscription at 5€/month, and I quickly realized that I had to have an account with at least 1000€ to keep up because my drawdown was too important.
I tried with 4 other signal providers before ending up with a margin call from my broker. I stopped trading after that.

## Fast forward to 2017
I am of age and can have my own trading account. I decided to get back into it but this time with a bot that will fetch orders on telegram and open them on my trading account. I had designed a bot that used regular expressions to extract orders from messages and put them in a database then the expert in MetaTrader would receive a message via WebSocket to open the orders. To maintain my terminal active I had to set up a quick and dirty solution with wine on Linux. The whole thing cost me 1,20€ per month for the hosting on a VPS. I tried to offer the service to some signal providers and was born Smashpips, a copy trading service for telegram signal providers. Unfortunately, this service was not very successful. I developed it for myself and not for the use cases of the providers. They had to be able to disable the service to bad payers, which meant they had to manage the billing via Smashpips. So I stopped trading again and stopped paying subscriptions to the signal providers.

## October 2020
Trading is booming in my home country. I launch the second version of Smashpips with a reinforcement learning algorithm that could allow me to get rid of the providers (Well the maths behind ML are quite huge, and I will never forget those who helped me understand some of it). I used to charge a monthly subscription fee of 39,99€. I had several users and at its peak, the service generated 3K€ of MRR for a working capital requirement of 160€ per month. No advertising, just emails and WhatsApp messages. But as with any business, when you do it just for the sake of profit without caring about user satisfaction, you hit a wall very quickly. Several competitors emerged and I lost almost all my users. I stopped the servers in February 2021 and decided to create Gryzle which would be the sum of everything I needed as well as the signal providers.
<div class="giphy-container"><iframe src="https://giphy.com/embed/dvZSDOywoCM4Sro65Q" width="100%" height="100%" style="position:absolute" frameBorder="0" allowFullScreen class="giphy-embed" ></iframe></div>

## July 2022
I have the first version of Gryzle online. A marketplace where signal providers could offer their signals, copy trading, and quotes, and manage their billing and customer relationships via mailing campaigns and landing pages. For a client, Gryzle is the assurance of having a signal provider with transparent results and a tailored user experience to simplify his trading life. All this is because I understand that using an excel file to check if all your clients are up to date with their payments is a very time and energy-consuming task. Also, I know that it is almost impossible to stay attentive in front of your Telegram app from morning to night to open orders as soon as an alert is received. Gryzle is for me the solution to automate these important but too long and frustrating tasks. Well next stop for Gryzle will be hopefully #TheMoon 🚀
<div class="giphy-container"><iframe src="https://giphy.com/embed/DnMMGxEvniha7CvASq" width="100%" height="100%" style="position:absolute" frameBorder="0" class="giphy-embed" allowFullScreen></iframe></div>

In my next articles, I'll document the whole creation process behind Gryzle and you'll know if this time the product will be adequate with the users' expectations. Or not.

I hope this helped you understand how Gryzle is born. If you have any questions, [please send me a little message](/contact). I will be happy to help you. If you liked this article, please show some love and share it with your friends. Thank you for reading!