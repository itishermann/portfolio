export interface DefaultPageProps {
	title?: string;
	description?: string;
	og?: OGMetaItem[];
}

export interface OGMetaItem {
	property: string;
	content: string;
}