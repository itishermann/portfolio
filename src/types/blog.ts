import { z } from "astro:content";
import { blogCollectionSchema } from "src/schemas/content";


export type BlogPostMeta = z.infer<typeof blogCollectionSchema>;

export interface BlogPostCardProps extends BlogPostMeta {
  url: string;
}
  