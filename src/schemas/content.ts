import {z} from "astro:content";

export const blogCollectionSchema = z.object({
    title: z.string(),
    subtitle: z.string(),
    layout: z.string().optional(),
    date: z.date(),
    tags: z.array(z.string()),
    cover: z.string().optional(),
    draft: z.boolean().optional(),
  });