import rss from '@astrojs/rss';
import type { APIContext } from "astro";
import {getCollection} from "astro:content";
import dayjs from "dayjs";
import {defaultBlogCover} from "../constants";
import sanitizeHtml from 'sanitize-html';
import MarkdownIt from 'markdown-it';
const parser = new MarkdownIt();
export async function get(context: APIContext) {
  const blog = await getCollection('blog');
  return rss({
    // `<title>` field in output xml
    title: 'Hermann Kao’s Blog',
    // `<description>` field in output xml
    description: 'A generalist’s blog about web development, design, and other things.',
    // Pull in your project "site" from the endpoint context
    // https://docs.astro.build/en/reference/api-reference/#contextsite
    site: context.site,
    // Array of `<item>`s in output xml
    // See "Generating items" section for examples using content collections and glob imports
    items: blog
        .sort((a, b) => dayjs(b.data.date).unix() - dayjs(a.data.date).unix())
        .map((post) => ({
      title: post.data.title,
      pubDate: post.data.date,
      description: post.data.subtitle,
      author: "Hermann Kao<hello@itishermann.me>",
      categories: post.data.tags,
      draft: post.data.draft,
      content: sanitizeHtml(parser.render(post.body)),
      enclosure: {
        url: post.data.cover || defaultBlogCover,
        type: 'image/jpeg',
        length: 1000,
      },
      // Compute RSS link from post `slug`
      // This example assumes all posts are rendered as `/blog/[slug]` routes
      link: `/blog/${post.slug}/`,
    })),
    // (optional) inject custom xml
    customData: `<language>en-us</language>`,
    stylesheet: '/rss/pretty-feed-v3.xsl',
  });
}