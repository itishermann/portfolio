import { defineConfig } from 'astro/config';
import sitemap from "@astrojs/sitemap"; // https://astro.build/config

import { astroImageTools } from "astro-imagetools";
import prefetch from "@astrojs/prefetch"; // https://astro.build/config


// https://astro.build/config
export default defineConfig({
  vite: {
    server: {
      watch: {
        ignored: ['**/.idea/**']
      }
    }
  },
  site: 'https://itishermann.me',
  integrations: [sitemap({
    changefreq: 'always',
    priority: 1
  }), prefetch(), astroImageTools]
});